from Account import Account


# скелет консольного приложения
class CLI:

    # при инициализации консольного предложения выводим приветственное сообщение
    # и вносим стандартные команды в массив команд
    def __init__(self, blockchain, public_key, private_key):
        self.commands = {}
        self.default_commands()
        self.welcome_message()
        self.blockchain = blockchain

        self.account = None
        self.authed = False

        if public_key and private_key:
            self.account = Account(public_key, private_key)
            self.authed = True

        self.blockchain.on('create_block', self.on_create_transaction)

    def on_create_transaction(self, tx):
        print('Новый блок!')
        print(tx)

    # выводим приветственное сообщение
    def welcome_message(self):
        print('Добро пожаловать в DinoCoin BlockChain')
        print('Введите /help для просмотра списка команд')

    # добавляем новую команду
    def add_command(self, command, handler, args=None, desc=''):
        self.commands[command] = {'handler': handler, 'args': args, 'description': desc}

    # отлавливаем введенную команду
    def handle_command(self, command, args):
        if command in self.commands:
            self.commands[command]['handler'](*args)
            return True
        else:
            return False

    # вносим стандартные команды
    def default_commands(self):
        self.add_command('/help', self.help_command, desc='Список всех команд')
        self.add_command('/stop', self.stop_command, desc='Остановка ноды')
        # self.add_command('/test', self.test_command, ['test_param'], 'Команда для проведения теста')
        self.add_command('/create_wallet', self.create_wallet, desc='Команда для создания нового аккаунта')
        self.add_command('/auth', self.auth_command, desc='Команда для активация кошелька')
        self.add_command('/send', self.send_command, desc='Команда для отправки монет')
        self.add_command('/mine', self.mine_coommand, desc='Запуск майнера')
        self.add_command('/balance', self.balance_command, desc='Просмотр баланса')
        self.add_command('/authors', self.authors_command, desc='Просмотр информации об авторах')
        self.add_command('/view_account', self.view_account, desc='Просмотр ключей аккаунта')
        self.add_command('/transactions', self.tx_command, desc='Просмотр активных транзакций')
        # self.add_command('/make_error_block', self.make_error_command(), desc='Создать невалидный блок (доступна только в консоли и для временного дебага)')

    def run(self):
        active = True
        while active:
            try:
                command = input()
                if command:
                    command_args = command.split()
                    if len(command_args) > 0:
                        cmd_name = command_args.pop(0).lower()

                    if not self.handle_command(cmd_name, command_args):
                        print('Команда не найдена')
            except (KeyboardInterrupt, EOFError):
                active = False
                print('Остановка ноды...')

    # тестовая команда
    def test_command(self, arg1):
        print('~_~ ->', arg1)

    # тестовая команда
    def view_account(self):
        print('Публичный ключ')
        print(self.account.get_public_key())
        print('Приватный ключ')
        print(self.account.get_private_key())

    # выводим справку по командам
    def help_command(self):
        print('--------ПОМОЩЬ--------')
        for cmd_name, data in self.commands.items():
            args = ''
            if 'args' in data and data['args']:
                args = ' '.join(data['args'])
            print(cmd_name, args, '-', data['description'])

    # создаем новый аккаунт
    def create_wallet(self):
        print('Создание ключей...')
        (public_key, private_key) = Account.create().keys()
        print('Сохраните ваши ключи. Без них вы не сможете получить доступ к вашему аккаунту')
        print('Открытый ключ (адрес аккаунта):', public_key)
        print('Закрытый ключ:', private_key)

    # переводим средства
    def send_command(self):
        if self.authed:
            print('Введите адрес кошелька, на который хотите перевести средства:')
            address = input()
            print('Введите сумму')
            amount = float(input())
            res = self.blockchain.create_new_transaction(self.account, address, amount)
            if not res is False:
                print('Транзакция успешно добавлена')
            else:
                print('У вас недостаточно монет')
        else:
            print('Ошибка: Прежде авторизируйтесь')

    # проходим аутентификацию 
    def auth_command(self):
        print('Введите ваш открытый ключ: ')
        public_key = input()
        print('Введите ваш закрытый ключ: ')
        private_key = input()
        self.account = Account(public_key, private_key)
        print('Авторизация прошло успешно')
        self.authed = True

    # # невалидный блок
    # def make_error_command(self):
    #     print('Введите номер блока: ')
    #     index = input()
    #     block = self.blockchain.chain[index]
    #     print('Введите новые данные: ')
    #     newdata = input()
    #     self.blockchain.make_error(block, newdata)
    #     print('Создание невалидных данных прошло успешно')

    def mine_coommand(self):
        if self.authed:
            self.blockchain.mine(self.account)
        else:
            print('Ошибка: Прежде авторизируйтесь')

    def balance_command(self):
        if self.authed:
            print('Ваш баланс:', self.blockchain.get_balance(self.account.get_public_key()), "DinoCoins|", self.blockchain.convert_to_rub(self.blockchain.get_balance(self.account.get_public_key())), "rub")
        else:
            print('Ошибка: Прежде авторизируйтесь')

    def stop_command(self):
        raise KeyboardInterrupt()

    def authors_command(self):
        print('-------- DinoCoin BlockChain --------')
        print('Данный блокчейн разработан студентами')
        print('Дмитровского института непрерывного образования')
        print('* Федоровым Михаилом Алексеевичем')
        print('* Сидоровым Данилом Михайловичем')

    def tx_command(self):
        print('Активные транзакции')
        print(self.blockchain.transactions)

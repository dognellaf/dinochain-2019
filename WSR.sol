pragma solidity ^0.5.13;

contract WSR {
    
    address payable admin;

    constructor () public {
        admin=msg.sender; 
    }
    
    mapping (address => uint32) addressToCountOfProperty;
    mapping (address => uint32) addressToCountOfPosting;
    mapping (address => uint32) addressToCountOfConsent;

    //страктура для движемого имущества
    struct Property {
        address payable OwnerOfProperty;
        string TypeOfObject;
        bool Piedge;
        string YearOfRelase;
        string GovermentNumber;
    }
    
    //структура для подтверждений
    struct Consent {
        address payable SenderAccount;
        int8 TypeOfConsent;
        uint IndexOfSendedProperty;
        uint32 IndexOfPosting;
        address payable RecipientAccount;
        string Data;
    }
    
    //структура объявления
    struct Posting {
        address payable Seller;
        string TypeOfObject;
        int TermOfAnnoncing;
        uint256 Cost;
        uint32 IndexOfSellingProperty;
    }
    
    //массивы для имущества, согласий и объявлений
    Property[] properties;
    Consent[] consents;
    Posting[] postings;
    
    //функция для проверки нынешнего админа
    function WhoAdmin() external view returns (address payable) 
    {
        return(admin);
    }
    
    //функция передачи прав админа
    function SetAdminAccount(address payable NewAdmin) external
    {
        if (admin == msg.sender)
        {
            admin = NewAdmin;
            emit NewAdminSetted(NewAdmin, msg.sender);
        }
        else
        {
            emit NewAdminNotSetted(NewAdmin, msg.sender);
        }
    }
    
    //функция изменения хозяина собственности от лица админа
    function SetOwner(address payable _NewOwner, uint _IndexOfProperty) external
    {
        if (msg.sender == admin) {
            addressToCountOfProperty[_NewOwner]++;
            addressToCountOfProperty[properties[_IndexOfProperty].OwnerOfProperty]--;
            properties[_IndexOfProperty].OwnerOfProperty = _NewOwner;
            emit NewOwnerSetted(_NewOwner, admin);
        }
        else
        {
            emit NewOwnerNotSetted(_NewOwner, msg.sender);
        }
    }
    
    //функция создания имущества от лица админа
    function CreateProperty(address payable _OwnerOfProperty, string calldata  _TypeOfObject, string calldata  _YearOfRelase, string calldata _GovermentNumber) external {
        
        if (admin == msg.sender)
        {
            uint _IndexOfProperty = properties.push(Property(_OwnerOfProperty, _TypeOfObject, false, _YearOfRelase, _GovermentNumber));
            addressToCountOfProperty[_OwnerOfProperty]++;
            emit NewProperyWasCreated(_IndexOfProperty, _OwnerOfProperty); 
        }
        else
        {
            emit NewProperyNotWasCreated(0, msg.sender); 
        }
    }
    
    //функция просмотрета всей собственности
    function ViewAllProperty() external view returns(uint32[] memory) {
        uint32[] memory propList = new uint32[](addressToCountOfProperty[msg.sender]);
        uint k = 0;
        for(uint32 i = 0; i < properties.length; i++)
        {
         if (properties[i].OwnerOfProperty == msg.sender) {
             propList[k] = i;
             k++;
         }   
        }
        return(propList);
    }

    //функция просмотра информации о любом объекте собственности любого пользователя
    function ViewInfoAboutProperty(uint _i) external view returns (
        address OwnerOfPropertyAnsw, 
        string memory TypeOfObject, 
        bool _Piedge, 
        string memory _YearOfRelase, 
        string memory _GovermentNumber
    ){
        return
        (
            properties[_i].OwnerOfProperty, 
            properties[_i].TypeOfObject, 
            properties[_i].Piedge, 
            properties[_i].YearOfRelase, 
            properties[_i].GovermentNumber
        );
    }
    
    //функция просмотра информации об объявлении
    function ViewInfoAboutPosting(uint _i) external view returns (
        address payable Seller,
        string memory TypeOfObject,
        int TermOfAnnoncing,
        uint256 Cost,
        uint32 IndexOfSellingProperty
    ){
        return
        (
            postings[_i].Seller, 
            postings[_i].TypeOfObject, 
            postings[_i].TermOfAnnoncing, 
            postings[_i].Cost, 
            postings[_i].IndexOfSellingProperty
        );
    }
    
    //функция просмотра информации о конкретном подтверждении
    function ViewInfoAboutConsent(uint _i) external view returns (
        uint _IndexOfConsent, 
        int8 _TypeOfConsent, 
        string memory Information, 
        address payable _RecipientAccount, 
        uint _IndexOfSendedProperty
    ){
        require(consents[_i].RecipientAccount == msg.sender); 
        return(
            _i, 
            consents[_i].TypeOfConsent, 
            consents[_i].Data, 
            consents[_i].RecipientAccount, 
            consents[_i].IndexOfSendedProperty
        );
    }
    
    //функция просмотра всех объявлений о продаже и аренде
    function ViewAllPostings() external view returns(uint32[] memory) {
        uint32[] memory _ListOfPostings = new uint32[](postings.length);
        uint k = 0;
        for(uint32 i = 0; i < postings.length; i++)
        {
            _ListOfPostings[k] = i;
            k++;
        }
        return(_ListOfPostings);
    }

    //функция дарения имущества
    function GiftProperty
    (
        uint _IndexOfProperty, 
        address payable _RecipientAccount, 
        string calldata  _Data
    ) external returns(bool) {
        if (properties[_IndexOfProperty].OwnerOfProperty == msg.sender) {
            uint _IndexOfConsent = consents.push(Consent(msg.sender, 0, _IndexOfProperty, 0, _RecipientAccount, _Data));
            addressToCountOfConsent[_RecipientAccount]++;
            emit NewConsentWasCreated(_IndexOfConsent, _RecipientAccount, _Data);
            return(true);
        }
        else
        {
            emit NewConsentNotWasCreated(0, _RecipientAccount, _Data);
            return(false);
        }   
    }
    
    //функция просмотра всех своих подтверждений
    function ViewAllConsents(int8 _TypeOfConsent) external view returns(uint32[] memory){
        require (addressToCountOfConsent[msg.sender] != 0);
        uint32[] memory NumbersOfConsents = new uint32[](addressToCountOfConsent[msg.sender]);
        uint k = 0;
        for(uint32 _i = 0; _i < consents.length; _i++){
            if ((consents[_i].RecipientAccount == msg.sender)&&(consents[_i].TypeOfConsent == _TypeOfConsent)) {
            NumbersOfConsents[k]=_i;
            k++;                
            }
        }
        return(NumbersOfConsents);
    }
    
    //функция просмотра всех своих подтверждений
    function CreateBuyConsent(uint32 _IndexOfPosting, string calldata _Data) external returns(bool){
        if (postings[_IndexOfPosting].Seller != msg.sender) 
        {
        uint256 _id = consents.push(Consent(msg.sender, 1, postings[_IndexOfPosting].IndexOfSellingProperty, _IndexOfPosting, postings[_IndexOfPosting].Seller, _Data));
        addressToCountOfConsent[msg.sender]++;
        emit BuyingConsentWasCreated(_id, _IndexOfPosting, postings[_IndexOfPosting].Seller, msg.sender, _Data);
        return(true);
        }
        else
        {
            emit BuyingConsentNotWasCreated(_IndexOfPosting, msg.sender);
        }
    }
    
    //функция принятия дарения
    function AcceptProperty(uint _IndexOfConsent) external {
        if ((consents[_IndexOfConsent].RecipientAccount == msg.sender)&&(consents[_IndexOfConsent].TypeOfConsent == 0)) {
            addressToCountOfProperty[msg.sender]++;
            addressToCountOfProperty[properties[consents[_IndexOfConsent].IndexOfSendedProperty].OwnerOfProperty]--;
            properties[consents[_IndexOfConsent].IndexOfSendedProperty].OwnerOfProperty = msg.sender;
            delete (consents[_IndexOfConsent]);
            emit PropertyWasAccepted(_IndexOfConsent, msg.sender);
        }
        else
        {
            emit PropertyNotWasAccepted(_IndexOfConsent, msg.sender);
        }
    }
    
    //функция принятия продажи
    function AcceptTraiding(uint _IndexOfConsent) external {
        if ((consents[_IndexOfConsent].RecipientAccount == msg.sender)&&(consents[_IndexOfConsent].TypeOfConsent == 1)) {
            addressToCountOfProperty[consents[_IndexOfConsent].SenderAccount]++;
            addressToCountOfProperty[msg.sender]--;
            consents[_IndexOfConsent].SenderAccount.transfer(postings[consents[_IndexOfConsent].IndexOfPosting].Cost);
            properties[consents[_IndexOfConsent].IndexOfSendedProperty].OwnerOfProperty = consents[_IndexOfConsent].SenderAccount;
            delete (consents[_IndexOfConsent]);
            emit PropertyWasSelled(consents[_IndexOfConsent].IndexOfSendedProperty, msg.sender, consents[_IndexOfConsent].SenderAccount, postings[consents[_IndexOfConsent].IndexOfPosting].Cost);
        }
        else
        {
            emit PropertyNotWasSelled(consents[_IndexOfConsent].IndexOfSendedProperty, msg.sender, consents[_IndexOfConsent].SenderAccount);
        }
    }
  
    //функция создания нового объявления продажи объекта
    function CreateSellingPosting (uint32 _IndexOfProperty, uint256 _CostOfObject, int _TermOfAnnoncing) external {
        if (properties[_IndexOfProperty].OwnerOfProperty == msg.sender)
        {
            uint _IndexOfPosting = postings.push(Posting(msg.sender, properties[_IndexOfProperty].TypeOfObject, _TermOfAnnoncing, _CostOfObject, _IndexOfProperty));
            emit PostingWasCreated(msg.sender, _IndexOfProperty, _IndexOfPosting, _CostOfObject);
        }
        else
        {
            emit PostingNotWasCreated(msg.sender, _IndexOfProperty, 0, _CostOfObject);
        }
    }
    
                //события принятия и отказа от собственности
    
    event PropertyWasAccepted
    (
        uint _IndexOfConsent, 
        address _RecipientAccount
    );
    
    event PropertyNotWasAccepted
    (

        uint _IndexOfConsent, 
        address _RecipientAccount
    );
    
                //события передачи и ошибки передачи прав админа
            
    event NewAdminSetted
    (
        address payable _NewAdmin, 
        address payable _OldAdmin
    );
    
    event NewAdminNotSetted
    (
        address payable _NewAdmin, 
        address payable _OldAdmin
    );
    
                //события передачи прав и ошибки передачи прав собственника
                
    event NewOwnerSetted
    (
        address payable _NewOwner, 
        address payable  _Who
    );

    event NewOwnerNotSetted
    (
        address payable _NewOwner, 
        address payable  _Who
    );
    
                //событие создания и ошибки создания нового объявления
                
    event PostingWasCreated
    (
        address _PostingOwner, 
        uint _IndexOfProperty, 
        uint _IndexOfPosting, 
        uint256 _CostOfObject
    );

    event PostingNotWasCreated
    (
        address _PostingOwner, 
        uint _IndexOfProperty, 
        uint _IndexOfPosting, 
        uint256 _CostOfObject
    );
    
                //событие создания и ошибки создания нового имущества
    
    event NewProperyWasCreated
    (
        uint _IndexOfProperty, 
        address _OwnerOfProperty
    );
    
    event NewProperyNotWasCreated
    (
        uint _IndexOfProperty, 
        address _OwnerOfProperty
    );
    
                //событие создания и ошибки создания нового подтверждения
    event NewConsentWasCreated
    (
        uint _IndexOfConsent, 
        address _RecipientAccount, 
        string _Data
    );
    
    event NewConsentNotWasCreated
    (
        uint _IndexOfConsent, 
        address _RecipientAccount, 
        string _Data
    );
    
                //событие отправки и ошибки отправки заявки на покупку
    
    event BuyingConsentWasCreated
    (
        uint _IndexOfConsent,
        uint _IndexOfPosting,
        address _Seller,
        address _Customer,
        string _Data        
    );
    
    event BuyingConsentNotWasCreated
    (
        uint _IndexOfPosting,
        address _Customer
    );
    
                //событие принятия или отказа от продажи собственности
                
    event PropertyWasSelled
    (
        uint _IndexOfProperty, 
        address _Seller,
        address _Customer,
        uint256 _Cost
    );
    
    event PropertyNotWasSelled
    (
        uint _IndexOfProperty, 
        address _Seller,
        address _Customer
    );
}
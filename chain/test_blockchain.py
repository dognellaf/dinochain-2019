import pytest
from blockchain import BlockChain

chain = [{"index":0,"miner":"","previous_hash":"","proof":338,"signature":"","timestamp":1574920766.8270488,"transactions":[]},{"index":1,"miner":"MEgCQQCIWcApoAqPQiWbZzR0aIZMimPpWrqkF/cbf1MUMq+fBsf5bsZXNBHVCBZISw6pflSsh8elSZeUD1PEzyGbBqhPAgMBAAE=","previous_hash":"578831f2e663037d8e539ecbbd2fa5fcee18a17d538170067081b6b612747b91","proof":41016,"signature":"goNEodZB5qZJbDD3hW/XKWiUOB5Eci6+/NNjreHXoeXmZ2b2TGoU0drj1XafNCfQUnxKgrFgrgP4XdYEmvc7DQ==","timestamp":1574920792.0468476,"transactions":[{"amount":10.0,"recipient":"MEgCQQDR0siCCx19Vta7josKXKPSRPc9QMvBBcM5CrkRg9R+qg9czBKt6apvXfZk2XNCrSR/bqpGvGaLF6bhqvaIra/TAgMBAAE=","sender":"MEgCQQCIWcApoAqPQiWbZzR0aIZMimPpWrqkF/cbf1MUMq+fBsf5bsZXNBHVCBZISw6pflSsh8elSZeUD1PEzyGbBqhPAgMBAAE=","signature":"Zd5MBu+j4tGS0N7EdLwkU0RBnLqEBAFiHjOSUj+DjwyWaP6IGlpEi10Xn/89dyCTUJzAv/CB6RbKziBAUiht1Q==","timestamp":1574920787.4786189}]}]

blockchain = BlockChain()


def test_verify_chain():
    blockchain.verify_chain(chain)

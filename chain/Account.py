import rsa
import binascii
from base64 import b64decode, b64encode


class Account(object):

    # инициализация аккаунта, который содержит закрытый и открытый ключ
    def __init__(self, public_key, private_key):
        public_key = self.decode_public_key(public_key)
        private_key = self.decode_private_key(private_key)
        self.public_key = public_key
        self.private_key = private_key

    # создаем закрытый и открытый ключ и возвращаем экземпляр
    @staticmethod
    def create():
        (pubkey, privkey) = rsa.newkeys(512)

        pubkey = Account.encode_key(pubkey)
        privkey = Account.encode_key(privkey)

        return Account(pubkey, privkey)

    # возвращаем открытый ключ
    def get_public_key(self):
        return self.encode_key(self.public_key)

    # возвращаем закрытый ключ
    def get_private_key(self):
        return self.encode_key(self.private_key)

    # возвращаем закрытый и открытый ключ объекта
    def keys(self):
        return self.encode_key(self.public_key), self.encode_key(self.private_key)

    # расшифровываем открытый ключ
    @staticmethod
    def decode_public_key(encoded_key):
        return rsa.PublicKey.load_pkcs1(b64decode(encoded_key), 'DER')

    # расшифровываем закрытый ключ
    @staticmethod
    def decode_private_key(encoded_key):
        return rsa.PrivateKey.load_pkcs1(b64decode(encoded_key), 'DER')

    # кодируем ключ
    @staticmethod
    def encode_key(key):
        return b64encode(key.save_pkcs1('DER')).decode('utf8')
        # return binascii.hexlify(key.save_pkcs1()).decode('utf8')

    # создаем подпись для блока или транзакции
    def sign(self, hash):
        return b64encode(rsa.sign(hash.encode('utf8'), self.private_key, 'SHA-1')).decode('utf8')

    # проверка подписи
    @staticmethod
    def verify(hash, signature, public_key):
        # проверяем подпись
        public_key = Account.decode_public_key(public_key)
        signature = b64decode(signature.encode('utf8'))
        try:
            rsa.verify(hash.encode('utf8'), signature, public_key)
            return True
        except rsa.pkcs1.VerificationError:
            return False
